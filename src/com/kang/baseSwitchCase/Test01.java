package com.kang.baseSwitchCase;

/**
 * @author kang
 * @date 2023/8/1
 */

/**
 * switch case
 */
public class Test01 {

    /**
     * 规则：
     *    变量类型：byte,short,int,char,String
     *    case标签必须是字符串常量或字面量
     *    每个case后跟一个比较值和冒号
     *    遇到break语句时，switch才会结束
     *    case语句不一定必须包含break语句
     *    default一般是switch的最后一个分支(可以是任何位置，但是最好是最后一个)
     *    default分支不需要break语句
     *    如果当前匹配成功的case语句块没有break语句，则后续所有case的值都会输出，如果后续的case语句有break，则会跳出判断
     * @param args
     */
    public static void main(String[] args) {

                char grade = 'C';

                switch(grade)
                {
                    case 'A' :
                        System.out.println("优秀");
                        break;
                    case 'B' :
                    case 'C' :
                        System.out.println("良好");
                        break;
                    case 'D' :
                        System.out.println("及格");
                        break;
                    case 'F' :
                        System.out.println("你需要再努力努力");
                        break;
                    default :
                        System.out.println("未知等级");
                }
                System.out.println("你的等级是 " + grade);
            }
}
