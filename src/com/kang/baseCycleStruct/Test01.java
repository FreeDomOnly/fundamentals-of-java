package com.kang.baseCycleStruct;

/**
 * @author kang
 * @date 2023/8/1
 */

/**
 *  循环结构
 */
public class Test01 {

    /**
     * 三种循环结构
     *    while
     *    do ... while
     *    for
     * @param args
     */
    public static void main(String[] args) {

        /**
         * while 循环
         * while(表达式){
         *     // 循环体
         * }
         * 每次循环都会首先判断表达式是否为true
         */
        int a1 = 1;
        while (a1 < 5){
            // 输出4次
            a1++;
            System.out.println("while循环...");
        }
        /**
         * do ... while 循环
         * do{
         *     // 循环体
         * }while(表达式)
         * 第一次进入循环不需要判断，之后进入循环都会先判断表达式是否为true
         */
         int a2 = 1;
         do {
             // 输出4次
             a2++;
             System.out.println("do ... while 循环");
         }while (a2 < 5);
        /**
         * for 循环
         * 普通for循环
         *    for(初始表达式;条件表达式;迭代表达式){
         *     循环语句;
         * }
         *    可以省略(分号必须写)
         *        初始化表达式：但是需要定义在for循环之前
         *        条件表达式：这样会出现死循环
         *        迭代表达式：
         * 增强for循环
         *    for(声明语句 : 表达式){
         *    //代码句子
         * }
         */
         int[] arr = {1,2,3,4};
         // 普通
            for (int i = 0; i < arr.length; i++) {
                System.out.println(arr[i]);
            }
        //增强for循环
        for (int a : arr){
            System.out.println(a);
        }
        /**
         * 相关关键字
         *     break：跳出整个循环(单个循环体)
         *            跳出最里层的整个循环(多层循环)
         *     continue：跳出本次循环
         */
        int a3 = 1;
         while (a1 < 5){
             if (a1 == 3){
                 break;
             }
             if (a1 == 2){
                 continue;
             }
             System.out.println("关键字 --- while循环");
         }
        /**
         * 只会输出一次："关键字 --- while循环"
         */
    }
}
