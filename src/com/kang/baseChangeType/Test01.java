package com.kang.baseChangeType;

/**
 * @author kang
 * @date 2023/7/25
 */

/**
 * 变量类型
 */
public class Test01 {
    /**
     * 成员变量（Instance Variables）：
     *    定义在类中、方法之外的变量，作用域为整个类，可以被类中的任何方法、
     *        构造方法和语句块访问。成员变量可以被访问修饰符修饰。
     *    成员变量在对象创建的时候创建，在对象被销毁的时候销毁
     *    成员变量可以声明在使用前或者使用后
     */
    // 成员变量
    private String name;

    /**
     * 静态变量（Class Variables）(类变量)：
     *     定义在类中、方法之外的变量，并且使用 static 关键字修饰，
     *         作用域为整个类，可以被类中的任何方法、构造方法和语句块访问，
     *         静态变量的值在程序运行期间只有一个副本。静态变量可以被访问修饰符修饰。
     *     它与类相关而不是与实例相关，即无论创建多少个类实例，
     *        静态变量在内存中只有一份拷贝，被所有实例共享。
     *     静态变量在类加载时被创建，在整个程序运行期间都存在
     *     静态变量可以用来存储整个程序都需要使用的数据，如配置信息、全局变量等
     *     如果一个静态变量依赖于另一个静态变量，那么它必须在后面定义
     *         public class MyClass {
     *              public static int count1 = 0;
     *              public static int count2 = count1 + 1;
     *         }
     *     静态变量需要考虑其线程安全性
     *     静态变量通常用于以下场景：
     *        存储全局状态或配置信息
     *        计数器或统计信息
     *        缓存数据或共享资源
     *        工具类的常量或方法
     *        单例模式中的实例变量
     */
    // 静态变量
    private static int age;


    /**
     * 参数变量（Parameters）：
     *    方法定义时声明的变量，作为调用该方法时传递给方法的值。
     *        参数变量的作用域只限于方法内部。
     */
    // String name 为参数变量
    public Test01(String name) {
        /**
         * 局部变量（Local Variables）：
         *    定义在方法、构造方法或语句块中的变量，作用域只限于当前方法、构造方法或语句块中。
         *       局部变量必须在使用前声明，并且不能被访问修饰符修饰。
         *    局部变量再执行完成之后会被销毁，其占用的内存也会被释放
         *    局部变量是在栈上分配的
         *    局部变量没有默认值，被声明后，必须经过初始化，才可以使用,
         *       如果没有初始化默认值，会编译报错
         */
        // 局部变量
        int age = 10;
        this.name = name;
        Test01.age = age;
        System.out.println("名字:"+this.name+"年龄:"+Test01.age);
    }

    // 引用数据类型
    public void updateName(Test01 test01){
        test01.name = "哈哈哈";
    }

    public void swap(int a){
        int temp = 0;
        a = temp;
    }



    @Override
    public String toString() {
        return "Test01{" +
                "name='" + name + '\''+
                '}';
    }

    /**
     * 方法参数的传递有两种
     *    值传递：
     *       在方法调用时，传递的是实际参数的值的副本。
     *          当参数变量被赋予新的值时，只会修改副本的值，不会影响原始值。Java 中的基本数据类型都采用值传递方式传递参数变量的值
     *    引用传递：
     *       在方法调用时，传递的是实际参数的引用（即内存地址）。
     *          当参数变量被赋予新的值时，会修改原始值的内容。
     *          Java 中的对象类型采用引用传递方式传递参数变量的值
     */

    public static void main(String[] args) {
        Test01 test01 = new Test01("张三");
        test01.updateName(test01);
        System.out.println(test01);// Test01{name='哈哈哈'}
        int a = 20;
        test01.swap(a);
        System.out.println(a); // 20
    }

}
