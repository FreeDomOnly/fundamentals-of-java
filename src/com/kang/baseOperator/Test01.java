package com.kang.baseOperator;

/**
 * @author kang
 * @date 2023/7/31
 */

/**
 * 运算符
 *
 */
public class Test01 {


    public static void main(String[] args) {

        /**
         * 算术运算符
         *    +
         *    -
         *    *
         *    /
         *    %：取余
         *    ++
         *      a++：先参加表达式运算然后再自增
         *      ++a：先自增然后参加表达式运算
         *    --
         *      a--：先参加表达式运算然后再自减
         *      --a：先自减然后参加表达式运算
         */

        /**
         * 关系运算符
         *    ==
         *    !=
         *    >
         *    <
         *    >=
         *    <=
         */

        /**
         * 逻辑运算符
         *    &&：当两侧都为真，为真
         *    ||：当一侧为真，为真
         *    !：非
         */

        /**
         * 赋值运算符
         *    =：
         *    +=：a+=b -> a = a + b
         *    -=：a-=b -> a = a-b
         *    *=：a*=b -> a = a * b
         *    /=：a/=b -> a = a / b
         *    %=：a%/b -> a = a % b
         */

        /**
         * 三元运算符
         *    expression ? (if true) value1 : (if false) value2
         */

        /**
         * instanceof 检查该对象是否是一个特定类型（类类型或接口类型）
         *    object1 instanceof object2 -> TRUE/FALSE
         *    例如 String name = ""; Boolean result = name instanceof String;->true
         */

        /**
         * java运算符的优先级
         *    后缀：(),[],'.'
         *    一元：expr++ expr--
         *    一元：++expr --expr + - ~ ！从右到左
         *    乘除：* / %
         *    加减：+ -
         *    关系：> >= < <=
         *    相等：== !=
         *    逻辑与：&&
         *    逻辑或：||
         *    条件：？：
         *    赋值：= += -= *= /= %=
         *    逗号：，
         */
    }
}
