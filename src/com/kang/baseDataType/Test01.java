package com.kang.baseDataType;

/**
 * @author kang
 * @date 2023/7/25
 */

/**
 * 基本数据类型
 */
public class Test01 {
    /**
     * 内置数据类型
     *   byte,short,int,float,double,long,char,boolean
     */

    public static void main(String[] args) {
        /**
         * 基本数据类型的取值范围，其值已经以常量的形式在对应的包装类中定义了
         * 在定义 float 类型时必须在数字后面跟上 F 或者 f
         */
                // byte
                System.out.println("基本类型：byte 二进制位数：" + Byte.SIZE);
                System.out.println("包装类：java.lang.Byte");
                System.out.println("最小值：Byte.MIN_VALUE=" + Byte.MIN_VALUE);
                System.out.println("最大值：Byte.MAX_VALUE=" + Byte.MAX_VALUE);
                System.out.println();

                // short
                System.out.println("基本类型：short 二进制位数：" + Short.SIZE);
                System.out.println("包装类：java.lang.Short");
                System.out.println("最小值：Short.MIN_VALUE=" + Short.MIN_VALUE);
                System.out.println("最大值：Short.MAX_VALUE=" + Short.MAX_VALUE);
                System.out.println();

                // int
                System.out.println("基本类型：int 二进制位数：" + Integer.SIZE);
                System.out.println("包装类：java.lang.Integer");
                System.out.println("最小值：Integer.MIN_VALUE=" + Integer.MIN_VALUE);
                System.out.println("最大值：Integer.MAX_VALUE=" + Integer.MAX_VALUE);
                System.out.println();

                // long
                System.out.println("基本类型：long 二进制位数：" + Long.SIZE);
                System.out.println("包装类：java.lang.Long");
                System.out.println("最小值：Long.MIN_VALUE=" + Long.MIN_VALUE);
                System.out.println("最大值：Long.MAX_VALUE=" + Long.MAX_VALUE);
                System.out.println();

                // float
                System.out.println("基本类型：float 二进制位数：" + Float.SIZE);
                System.out.println("包装类：java.lang.Float");
                System.out.println("最小值：Float.MIN_VALUE=" + Float.MIN_VALUE);
                System.out.println("最大值：Float.MAX_VALUE=" + Float.MAX_VALUE);
                System.out.println();

                // double
                System.out.println("基本类型：double 二进制位数：" + Double.SIZE);
                System.out.println("包装类：java.lang.Double");
                System.out.println("最小值：Double.MIN_VALUE=" + Double.MIN_VALUE);
                System.out.println("最大值：Double.MAX_VALUE=" + Double.MAX_VALUE);
                System.out.println();

                // char
                System.out.println("基本类型：char 二进制位数：" + Character.SIZE);
                System.out.println("包装类：java.lang.Character");
                // 以数值形式而不是字符形式将Character.MIN_VALUE输出到控制台
                System.out.println("最小值：Character.MIN_VALUE="
                        + (int) Character.MIN_VALUE);
                // 以数值形式而不是字符形式将Character.MAX_VALUE输出到控制台
                System.out.println("最大值：Character.MAX_VALUE="
                        + (int) Character.MAX_VALUE);

        /**
         * 引用类型
         *    对象,数组是引用类型
          */

        /**
         * java常量
         *     使用 final 关键字来修饰常量
         *     通常常量名使用大写字母拼接的单词来表示
         */

        /**
         * 自动类型转换
         *    低  ------------------------------------>  高
         *    byte,short,char—> int —> long—> float —> double
         *    转换规则：
         *       不能对boolean类型进行转换
         *       不能把对象类型转换成不相关类的对象
         *       容量大的类型转换成容量小的类型时必须使用强制类型转换
         *          转换过程可能导致溢出或者损失精度
         *       浮点型到整数的转换是直接舍弃小数位，而不是四舍五入
         *       转换前的数据类型的位数小于转换后的数据类型的位数，可以实现自动类型转换
         *    转换格式：
         *       (type) value type是value转成的类型
         */

    }
}
