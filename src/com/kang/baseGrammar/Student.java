package com.kang.baseGrammar;

/**
 * @author kang
 * @date 2023/7/25
 */

import java.util.ArrayList;
import java.util.List;

/**
 *  对象：是类的一个实例
 *       例如：狗的一个对象(实例) -> 颜色：黄色；名字：豆包
 *  类：对具有共性事物的一种抽象(抽取共性的方法(或属性))
 */
public class Student {
    /**
     * 全局变量(成员变量)：
     *    定义在类中方法外,可以在类中的任何一处使用,可以 this.属性 来调用
     * 局部变量：
     *    定义在方法内,只能在所定义的方法中使用
     */

    /**
     * 大小写敏感: Hello 和 hello 是不一样的
     * 类名：首字母大写，之后的每个单词的首字母也大写。例如：MyStudent
     * 方法名：首字母小写，之后的每个单词的首字母大写。例如：getName()
     * 源文件名：必须与类名一样，(.java)为后缀
     * 主方法的入口：public static void mian(String[] args){}方法开始执行的
     */

    /**
     * java标识符
     *     所有的标识符应该以字母，美元符，下划线开始;
     *     首字符之后的所有字符可以是字母，数字，下划线，美元符来组合;
     *     关键字不能作为标识符;
     *     标识符也是大小写敏感的.
     */

    /**
     * java 修饰符(修饰类中的方法和属性)
     * 访问权限修饰符：public,default,protected,private
     * 非访问控制修饰符：final,abstract,static,synchronized
     */

    /**
     * 变量
     *    局部变量：
     *       变量声明和初始化都是在方法中，方法结束后，变量就会自动销毁
     *    成员变量(非静态变量)
     *       这种变量在创建对象的时候实例化。成员变量可以被类中方法、
     *       构造方法和特定类的语句块访问
     *    类变量(静态变量)
     *       类变量也声明在类中，方法体之外，但必须声明为 static 类型
     */

    /**
     * java的null 不是关键字，只是一个字面常量
     */

    /**
     * 构造方法
     *    每个类都有一个构造方法，如果没有显式地为类定义方法，java编译器会
     *       为该类提供一个默认的无参构造方法
     *    构造方法名必须与类同名，没有返回值
     */

    /**
     * 创建对象
     *    声明：声明一个对象，包括对象的类型和名称；例如 Student student
     *    实例化：使用new关键字创建对象；
     *    初始化：使用new关键字实例化对象时，或调用构造方法初始化对象。
     */

    /**
     * 源文件声明规则
     *    一个源文件只能有一个public类
     *    一个源文件可以有多个非public类
     *    如果一个类定义在某个包中，那么package语句应该在源文件的首行
     *    如果源文件中有import语句，那么应该在package语句和类定义之间，
     *       如果没有package，那么import语句应该在首行
     */


    // 属性
    private String name;

    private int age;

    // 无参构造
    public Student() {
    }

    // 有参构造
    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    // 方法(行为)
    public void study(){
        // 局部变量
        int year = 3;
        System.out.println(this.name+"已经在学校学习"+year+"年了");
    }

    public static void sleep(){
        System.out.println("休息了...");
    }

    public void setName(String name){
        this.name = name;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    public static void main(String[] args) {
        /**
         * public : 访问权限修饰符
         * static : 关键字
         * void : 返回值
         * main : 方法名
         * args : 参数
         */
        Student student = new Student("张三",16);
        // 对象.方法();
        student.study(); // 返回：张三已经在学校学习3年了
        // 对于关键字static修饰的方法,使用类名.方法()/对象.方法();
        Student.sleep();// 休息了...
        student.sleep();// 休息了...
    }


}
