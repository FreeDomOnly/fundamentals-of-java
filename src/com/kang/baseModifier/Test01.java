package com.kang.baseModifier;

/**
 * @author kang
 * @date 2023/7/31
 */

/**
 * 修饰符
 *    修饰符用来定义类、方法或者变量
 *    Java 程序的 main() 方法必须设置成公有的，否则，Java 解释器将不能运行该类
 */
public class Test01 {

    /**
     * 分类
     *    1.访问修饰符
     *      default (即默认，什么也不写): 在同一包内可见，不使用任何修饰符。使用对象：类、接口、变量、方法。
     *
     *      private : 在同一类内可见。使用对象：变量、方法。 注意：不能修饰类（外部类）
     *
     *      public : 对所有类可见。使用对象：类、接口、变量、方法
     *
     *      protected : 对同一包内的类和所有子类可见。使用对象：变量、方法。 注意：不能修饰类(外部类)(内部类除外)
     *    2.非访问修饰符
     *      static修饰符：修饰类和类变量
     *      final：修饰类(不能被继承) 修饰方法(不能被子类重新定义,但是可以被子类继承) 修饰变量(为常量)
     *      abstract：创建抽象类和抽象方法
     *        如果一个类中有抽象方法(被abstract修饰的方法)，那么该类一定要被声明为抽象类(被abstract修饰的类)
     *        抽象方法：
     *           一种没有任何实现方法的方法
     *           不能被final和static修饰
     *           任何继承抽象类的子类必须实现抽象类的所有抽象方法，除非该类也是抽象类
     *           抽象类可以不包含抽象方法
     *           抽象方法的声明已分号结尾
     *      synchronize/volatile：线程-锁
     *        synchronized 关键字声明的方法同一时间只能被一个线程访问
     *        volatile 修饰的成员变量在每次被线程访问时，都强制从共享内存中重新读取该成员变量的值。这样在任何时刻，两个不同的线程总是看到某个成员变量的同一个值。
     * @param args
     */

    /**
     * 访问控制和继承
     *     父类中声明为public的方法在子类中也必须声明为public
     *     父类中声明为protected的方法在子类中要么声明为protected 要么声明为public 不可声明为private
     *     父类中声明为private的方法不能被继承
     * @param args
     */

    public static void main(String[] args) {

    }
}
